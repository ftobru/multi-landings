package main

import (
	"io/ioutil"
	"os"
	"strings"
	"github.com/fatih/color"
	"fmt"
)

const (
	TYPE_TEMPLATE 		= 0
	TYPE_AB_SPLIT 		= 1
	TYPE_GEO_SPLIT 		= 2
	LANDING_NOT_FOUND	= 404
	LANDING_ERROR		= 500
	LANDING_NORMAL		= 200
)

type Handler interface {
	Handle(query Query) string
}

type GetLandinger interface {
	GetLanding(lb LinkBase) Landing
}

type Template struct {
	Landing
}

func (t *Template) GetLanding(lb *LinkBase) Landing {
	// Получит шаблон
	(*t).Path = lb.Path.String
	common.DebugPrint("./files/" + lb.Path.String)
	if _, err := os.Stat("./files/" + lb.Path.String); os.IsNotExist(err) {
		common.DebugPrint("Landing not exist")
		return t.Landing
	}
	body, err := ioutil.ReadFile("./files/" + lb.Path.String)
	common.InitError(err)
	(*t).Body = string(body)
	return t.Landing
}

type AbSplit struct {
	Landing
}

func (ab *AbSplit) GetLanding(lb LinkBase) Landing {
	return Landing{}
}


type GeoSplit struct {
	Landing
}

func (gs *GeoSplit) GetLanding(lb LinkBase) Landing {
	//@todo
	return Landing{}
}

type LandingService struct {}

/**
 * Service landing
 */
func (service *LandingService) Handle(query *Query) Landing {
	var landing Landing
	common.DebugPrint("Url: " + query.URL)
	common.DebugPrint("Domain: " + query.Domain)
	common.DebugPrint("LinkId: " + string(query.LinkId))

	if !service.CheckDomain(query.Domain) {
		return service.SetStatus(landing, LANDING_NOT_FOUND)
	}
	if !service.CheckId(query.LinkId, "LinkId") {
		// 1. Если к нам не приходит link_id, то мы знаем, что это link_base
		domain := NewDomain()
		domain = domain.GetByName(query.Domain)

		if !service.CheckId(domain.Id, "Domain") {
			return service.SetStatus(landing, LANDING_NOT_FOUND)
		}
		common.DebugPrint("Domain name - " + string(domain.Name))
		link_base := service.GetLinkBaseByDomainIdAndUrl(int64(domain.Id), query.URL)

		if !service.CheckId(link_base.Id, "LinkBase") {
			return service.SetStatus(landing, LANDING_NOT_FOUND)
		}
		landing = service.GetLandingByLinkBase(link_base)

	} else {
		// 2. В противном случае это links
		link := NewLink().Find(query.LinkId)
		landing = service.GetLandingByLink(link)
	}
	landing = service.Compile(landing)
	return landing
}

func (s LandingService) SetStatus(landing Landing, status int) Landing {
	landing.Status = status
	return landing
}

// time replace
// geo replace
// insert link
// replace link
// product link
// param replace
func (s LandingService) Compile(l Landing) Landing {
	insert_l 	:= NewInsertLink().GetByParam(query.LinkInsert)
	time_r		:= NewReplacementTime().GetCompanyId(l.CompanyId)
	product_r  	:= NewReplaceProduct().GetByKey(query.ProductReplace)
	replace_l 	:= NewRaplacementLink().GetByParam(query.LinkReplace)
	params_r	:= NewParams().GetByCompanyId(l.CompanyId)

	color.Cyan("--------------------------------------------->")
	if common.Log() {
		fmt.Println(query)
		fmt.Println(insert_l)
		fmt.Println(time_r)
		fmt.Println(product_r)
		fmt.Println(replace_l)
		fmt.Println(params_r)
	}
	color.Cyan("--------------------------------------------->")

	if replace_l.Id != 0 {
		l.LinkReplace(replace_l)
	}
	if insert_l.Id != 0 {
		l.InsertReplace(insert_l)
	}
	if len(time_r) > 0 {
		for _, time := range time_r {
			l.TimeReplace(time)
		}
	}

	if product_r.Id != 0 {
		product := NewProduct().Find(product_r.ProductId)
		if product.Id != 0 {
			l.ProductReplace(product)
		}
	}
	if len(params_r) > 0 {
		l.ParamReplace(params_r)
	}
	return l
}

func (s LandingService) CheckUrl(url string) bool {
	if url == "" {
		common.DebugPrint("Url is empty")
		return false
	} else {
		common.DebugPrint("Url - " + url)
		return true
	}
}

func (s *LandingService) GetLinkBaseByDomainIdAndUrl(domainId int64, url string) *LinkBase {
	link_base := NewLinkBase()
	if s.CheckUrl(url) {
		link_base = link_base.GetByDomainIdAndUrl(domainId, query.URL)
	} else {
		link_base = link_base.GetByDomainId(domainId)
	}
	return link_base
}

// Проверить домен
func (s *LandingService) CheckDomain(domainName string) bool {
	if domainName == "" {
		common.DebugPrint("Domain name in not request")
		return false
	} else {
		common.DebugPrint("Domain name is normal")
		return true
	}
}

// Проверя идентификатор на наличие
func (sl *LandingService) CheckId(id int64, entityName string) bool {
	if id == 0 {
		common.DebugPrint(entityName + " not found")
		return false;
	} else {
		common.DebugPrint(entityName + " - id")
		return true
	}
}

func (s *LandingService) GetLandingByLink(l *Link) Landing {
	var landing Landing
	// Получит шаблон
	common.DebugPrint("./files/" + l.Path)
	if _, err := os.Stat("./files/" + l.Path); os.IsNotExist(err) {
		common.DebugPrint("Landing not exist")
		return landing
	}
	body, err := ioutil.ReadFile("./files/" + l.Path)
	common.InitError(err)
	landing.Body = string(body)
	landing.Status = LANDING_NORMAL
	return landing
}

func (s *LandingService) GetLandingByLinkBase(lb *LinkBase) Landing {
	var landing Landing
	var template Template
	if s.IsTemplate(lb) {
		landing = template.GetLanding(lb)
		landing = s.SetStatus(landing, LANDING_NORMAL)
	} else if s.IsAbSplit(lb) {
		//@todo Ab split
		landing = Landing{}
	} else if s.IsGeoSplit(lb) {
		// @todo Geo split
		landing = Landing{}
	} else {
		common.DebugPrint("Status link_base - not fount")
		landing = s.SetStatus(landing, LANDING_ERROR)
	}
	return landing
}

func (service *LandingService) IsTemplate(lb *LinkBase) bool {
	return lb.Type == TYPE_TEMPLATE
}

func (service *LandingService) IsAbSplit(lb *LinkBase) bool {
	return lb.Type == TYPE_AB_SPLIT
}

func (service *LandingService) IsGeoSplit(lb *LinkBase) bool {
	return lb.Type == TYPE_GEO_SPLIT
}

type StaticService struct {}

func (service *StaticService) Handle(query *Query) string {
	link_base := NewLinkBase()
	domain := NewDomain()
	domain = domain.GetByName(query.Domain)
	if domain.Id == 0 {
		common.DebugPrint("Domain not found")
	} else {
		link_base = link_base.GetByDomainId(domain.Id)
		if link_base.Id == 0 {
			common.DebugPrint("Link base not found")
		} else {
			path_static_file := strings.Split(link_base.Path.String, "/")
			content := "./files/" + strings.Join(path_static_file[:len(path_static_file) - 1], "/") + "/"
			return content
		}
	}

	return ""
}
