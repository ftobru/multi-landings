package main

import (
	"regexp"
	"net/url"
	"strconv"
)

type RedisConfig struct {
	RedisServer 		string 	`default:"localhost:6379"`
	DB					int64
	Collection			string	`default:"landings"`
}

type Query struct {
	Domain				string
	URL					string
	LinkId				int64
	LinkReplace			string
	LinkInsert			string
	ProductReplace		string
	CityId				int8
}

/**
 * Create link struct.
 * replacement_link_*
 * product
 * insert_link_*
 * link_id
 */
func (q *Query) MapRequest(host map[string]string, request url.Values) *Query {

	for key, value := range request {
		// Insert link
		if result, _ := regexp.MatchString("(insert_link_)[*a-z0-9_]+", key); result {
			q.LinkInsert = value[0]
			// Replacement link
		} else if result, _ := regexp.MatchString("(replacement_links_)[*a-z0-9_]+", key); result {
			q.LinkReplace = value[0]
		} else if key == "link_id" {
			q.LinkId, _ = strconv.ParseInt(value[0], 0, 32)
		} else if key == "prod" {
			q.ProductReplace = value[0]
		}
	}
	if value, isset := host["domain"]; isset {
		q.Domain = value
	}
	if value, isset := host["path"]; isset {
		q.URL = value
	}
	return q
}


type DbConfig struct {
	Host 				string	`default:"localhost"`
	User 				string	`default:"tools"`
	Password			string	`default:"tools"`
	Database			string	`default:"tools"`
}



