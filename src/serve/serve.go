package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"github.com/joho/godotenv"
	"log"
	"github.com/fatih/color"
	"flag"
	"io/ioutil"
	"html/template"
	"fmt"
)

const (
	DEFAULT_ENV = ".env"
	DEFAULT_HOST = "http://modulcrm.ru"
)

var (
	common = Common{}
	env = flag.String("env", DEFAULT_ENV, "Environment file")
	dbg = flag.Bool("debug", false, "Debug")
	query = Query{}
	landing_service = LandingService{}
	static_service  = StaticService{}
	number_request = 0
	driver = Driver{}
)

func Init() {
	// Configure application
	flag.Parse()
	// Debug
	common.SetLog(*dbg)
	// Environment file config
	LoadEnv(env)
	DebugEnableMessage(*dbg)

	color.Green("Connect db..")
	driver.ConnectDb()
	color.Green("Connect db - success!")

	color.Green("Connect redis..")
	driver.ConnectRedis()
	color.Green("Connect redis server - success!")

}

/**
 * Main function. Multi-landing service
 */
func main() {
	Init()

	router := mux.NewRouter()
	color.Green("Configure routing...")
	domainRouter := router.Host("{domain:[a-z0-9.:]+}").Subrouter()
	domainRouter.HandleFunc("/", LandingHandler)
	domainRouter.HandleFunc("/{path:[a-z0-9]+}", LandingHandler)
	domainRouter.HandleFunc("/{path:[a-z0-9/-].+}.{ext:(css|js|jpg|png|gif|ico|mp4|ttf|woff|woff2|eot)}", StaticsHandler)
	http.Handle("/", domainRouter)
	color.Green("Port 6446")
	color.Green("Start server...")
	http.ListenAndServe(":6446", nil)
}

func DebugEnableMessage(enable bool) {
	common.DebugPrint("Debug enable")
}


func LoadEnv(env *string) {
	common.DebugPrint("File environment - " + *env)
	err := godotenv.Load(*env)
	if err != nil {
		log.Fatal("Error loading " + *env + " file")
	}
}

/**
 * Static service. Output static files - css, jpg, js, gif, ico, mp4, ttf
 * `return:static`
 */
func StaticsHandler(w http.ResponseWriter, request *http.Request) {
	host := mux.Vars(request)
	q := query.MapRequest(host, request.URL.Query())
	number_request = number_request + 1
	// Убираем рудимент
	//q.URL = ""
	number_request_string := string(number_request)
	common.DebugPrint("= Start Request static" + number_request_string + "================================")
	static_file := static_service.Handle(q)
	common.DebugPrint(static_file + host["path"] + "." + host["ext"])
	static_file_byte, _ := ioutil.ReadFile(static_file + "/" + host["path"] + "." + host["ext"])
	common.DebugPrint("= End Request  static" + number_request_string + "================================")
	w = SetStaticHeader(w, host["ext"])
	w.Write(static_file_byte)
}

func SetStaticHeader(w http.ResponseWriter, ext string) http.ResponseWriter  {
	if ext == "css" {
		w.Header().Set("Content-type", "text/css")
	} else if ext == "jpg" {
		w.Header().Set("Content-type", "image/jpeg")
	} else if ext == "png" {
		w.Header().Set("Content-type", "image/png")
	} else if ext == "js" {
		w.Header().Set("Content-type", "application/x-javascript")
	} else if ext == "gif" {
		w.Header().Set("Content-type", "image/gif")
	} else if ext == "mp4" {
		w.Header().Set("Content-type", "video/mp4")
	} else if ext == "ttf" {
		w.Header().Set("Content-type", "application/x-font-ttf")
	} else if  ext == "woff" {
		w.Header().Set("Content-type", "application/font-woff")
	} else if ext == "eot" {
		w.Header().Set("Content-type", "application/vnd.ms-fontobject")
	} else if ext == "svg" {
		w.Header().Set("Content-type", "")
	} else if ext == "woff2" {
		w.Header().Set("Content-type", "application/font-woff2")
	} else {
		w.Header().Set("Content-type", "text/html")
	}

	return w
}


/**
 * Landing handler. Response HTTP 200. Output html
 * `return:html`
 */
func LandingHandler(w http.ResponseWriter, request *http.Request) {
	host := mux.Vars(request)
	q := query.MapRequest(host, request.URL.Query())
	number_request = number_request + 1
	common.DebugPrint("= Start Request" + string(number_request) + "================================")
	landing := landing_service.Handle(q)
	common.DebugPrint("= End Request  " + string(number_request) + "================================")
	fmt.Println(landing.Status)
	if landing.Status == LANDING_NOT_FOUND {
		renderTemplate(w, "404")
	} else if landing.Status == LANDING_ERROR {
		renderTemplate(w, "500")
	} else if landing.Status == LANDING_NORMAL {
		w.Write([]byte(landing.Body))
	} else {
		http.Redirect(w, request, DEFAULT_HOST, 301)
	}
}

func renderTemplate(w http.ResponseWriter, tmpl string) {
	t, _ := template.ParseFiles("./html/" + tmpl + ".html")
	t.Execute(w, Landing{})
}

