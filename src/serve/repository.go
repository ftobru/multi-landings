package main


import (
	_ "github.com/lib/pq"
	"github.com/jmoiron/sqlx"
	"database/sql"
	"time"
	"os"
	"strconv"
	"gopkg.in/redis.v3"
	"strings"
)

/**
 * Driver initial connection database
 */
type Driver struct {
	db			*sqlx.DB
	redisClient *redis.Client
	dbConfig	DbConfig
	redisConfig	RedisConfig
}


func (d *Driver) ConnectDb() {

	dbConfig := DbConfig{}
	dbConfig.Host = os.Getenv("DB_HOST")
	dbConfig.Database = os.Getenv("DB_DATABASE")
	dbConfig.User = os.Getenv("DB_USER")
	dbConfig.Password = os.Getenv("DB_PASSWORD")
	d.dbConfig = dbConfig
	d.db, _ = GetDb(dbConfig)
}

func (d *Driver) ConnectRedis() {
	redisConfig := RedisConfig{}
	redisDb, _ := strconv.ParseInt(os.Getenv("REDIS_DB"), 0, 64)
	redisConfig.DB = redisDb
	redisConfig.RedisServer = os.Getenv("REDIS_SERVER")
	redisConfig.Collection = os.Getenv("REDIS_COLLECTION")
	d.redisConfig = redisConfig
	d.redisClient = GetRedisClient(redisConfig)
}

func GetRedisClient(config RedisConfig) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr: config.RedisServer,
		DB: config.DB,
	})
}

func GetDb(config DbConfig) (*sqlx.DB, error) {
	d, err := sqlx.Connect("postgres", strings.Join([]string{
		strings.Join([]string{"host",  config.Host}, "="),
		strings.Join([]string{"user", config.User}, "="),
		strings.Join([]string{"password", config.Password}, "="),
		strings.Join([]string{"dbname", config.Database}, "="),
		strings.Join([]string{"sslmode", "disable"}, "="),
	}, " "))
	return d.Unsafe(), err
}





type Company struct {
	Id 					int64	`db:"id"`
	CreatedAt			string	`db:"created_at"`
	Driver
}

func (c *Company) Find(id int64) *Company {
	c.db.Get(&*c, "SELECT * FROM companies WHERE id = $1", id)
	return c
}

func NewCompany() *Company {
	return &Company{Driver: driver}
}

type Params struct {
	Tag					string	`db:"tag"`
	Meaning				string	`db:"meaning"`
	Driver
}

func (p *Params) GetByCompanyId(companyId int64) []Params {
	var common Common
	params, err := p.db.Query("SELECT tag, meaning FROM replacement_params WHERE company_id = $1", companyId)
	common.InitError(err)
	rows := []Params{}
	for params.Next() {
		var row Params
		params.Scan(&row.Tag, &row.Meaning)
		rows = append(rows, row)
	}
	return rows
}

func NewParams() *Params {
	return &Params{Driver: driver}
}


type ReplacementGeo struct {
	Phone				string	`db:"phone"`
	Address 			string	`db:"address"`
	Driver
}

/**
 * Получить гео-замены
 */
func (r *ReplacementGeo) GetByCompanyIdAndCityId(companyId int64, cityId int8) *ReplacementGeo {
	r.db.Get(&*r,
		"SELECT phone, address FROM replacement_geo WHERE company_id = $1 AND data ?& array[$2]",
		companyId, cityId)
	return r
}

func NewReplacementGeo() *ReplacementGeo {
	return &ReplacementGeo{Driver: driver}
}

type ReplacementTime struct {
	Id 					int64	`db:"id"`
	Name				string	`db:"name"`
	Description			string	`db:"description"`
	Tag 				string	`db:"tag"`
	StartTime			string	`db:"start_time"`
	FinishTime			string	`db:"finish_time"`
	Meaning				string	`db:"meaning"`
	Driver
}


/**
 * Получить тайм-замена
 */
func (r *ReplacementTime) GetCompanyId(companyId int64) []ReplacementTime {
	var rt []ReplacementTime
	r.db.Select(&rt,
		"SELECT * FROM replacement_time WHERE compnay_id = $1 AND start_time < CURRENT_TIME AND finish_time > CURRENT_TIME",
		companyId)
	return rt
}
/**
 * ReplaceTime
 */
func NewReplacementTime() *ReplacementTime {
	return &ReplacementTime{Driver: driver}
}

/**
 * Form
 */
type Form struct {
	Name				string	`db:"name"`
	IdAttr				string	`db:"id_attr"`
	FormType			string	`db:"form_type"`
	CallbackType		string	`db:"callback_type"`
	CallbackUrl			string	`db:"callback_url"`
	CallbackText		string	`db:"callback_text"`
	AfterHeaderText		string	`db:"after_header_text"`
	HeaderText			string	`db:"header_text"`
	BeforeHeaderText	string	`db:"before_header_text"`
	AfterBtnText		string	`db:"after_btn_text"`
	BtnText				string	`db:"brn_text"`
	BeforeBtnText		string	`db:"before_btn_text"`
	BtnColor			string	`db:"btn_color"`
	Fields				string	`db:"fields"`
	Driver
}

/**
 * Получить формы захвата
 */
func (f *Form) Find(id int64) *Form {
	f.db.Select(&*f, "SELECT *FROM landing_forms WHERE id = $1 AND ", id)
	return f
}


func NewForm() *Form {
	return &Form{Driver: driver}
}

type LinkBase struct {
	Id	 				int64			`db:"id"`
	Name				sql.NullString	`db:"name"`
	Description			sql.NullString	`db:"description"`
	Type				int8			`db:"type"`
	CompanyId			int8			`db:"company_id"`
	CreatedAt			time.Time		`db:"created_at"`
	UpdatedAt			time.Time		`db:"updated_at"`
	DeletedAt			sql.NullString	`db:"deleted_at"`
	DomainId			int8			`db:"domain_id"`
	Url 				sql.NullString	`db:"url"`
	OpenUrl				bool			`db:"open_url"`
	LinkIds				sql.NullString	`db:"link_ids"`
	CitiesIds			sql.NullString	`db:"cities_idxs"`
	Path 				sql.NullString	`db:"path"`
	GoogleAnalytics		sql.NullString	`db:"google_analytics"`
	YaMetrica			sql.NullString	`db:"ya_metrika"`
	Driver
}

func (lb *LinkBase) GetByDomainIdAndUrl(domainId int64, url string) *LinkBase {
	// these can be combined:
	arg := []interface{} {
		domainId,
		url,
	}
	_ = lb.db.Get(&*lb, "SELECT * FROM link_bases WHERE domain_id = $1 AND deleted_at IS NULL AND url = $2", arg...)
	//Common.ErrorPrint(err)
	return  lb
}

func (lb *LinkBase) GetByDomainId(domainId int64) *LinkBase {
	_ = lb.db.Get(&*lb, "SELECT * FROM link_bases WHERE domain_id = $1 AND deleted_at IS NULL", domainId)
	//Common.ErrorPrint(err)
	return lb
}


func NewLinkBase() *LinkBase {
	return &LinkBase{Driver: driver}
}

type Link struct {
	Id					int64				`db:"id"`
	Name				sql.NullString		`db:"name"`
	Link				sql.NullString		`db:"link"`
	ShortLink			sql.NullString		`db:"short_link"`
	Status				int8				`db:"status"`
	Hosts				int64				`db:"hosts"`
	Hits				int64				`db:"hits"`
	CountLeads			int64				`db:"count_leads"`
	Convert				float32				`db:"convert"`
	CompanyId			int32				`db:"company_id"`
	InsertLink			sql.NullString		`db:"insert_link"`
	ReplacementLink		sql.NullString		`db:"replacement_link"`
	ReplacementProduct	sql.NullString		`db:"replacement_product"`
	Type				int8				`db:"type"`
	DomainId			int8				`db:"domain_id"`
	LinkBaseId			int					`db:"link_base_id"`
	Path 				string				`db:"path"`
	Url 				sql.NullString		`db:"url"`
	OpenUrl				bool				`db:"opent_url"`
	Cities				sql.NullString		`db:"cities"`
	Utm 				sql.NullString		`db:"urm"`
	tag 				sql.NullString		`db:"tag"`
	form_id				int64				`db:"form_id"`
	Driver
}

func (l *Link) Find(id int64) *Link {
	//common.DebugPrint(fmt.Sprintf("SELECT * FROM links WHERE id = %int64 AND deleted_at = NULL", id))
	err := l.db.Get(&*l, "SELECT * FROM links WHERE id = $1 AND deleted_at IS NULL", id)
	common.InitError(err)
	return l
}

func (l *Link) GetOneByLinkBase(id int64) *Link {
	l.db.Get(&*l, "SELECT * FROM links WHERE link_base_id = $1 AND deleted_at = NULL")
	if l.Id == 0 {
		common.DebugPrint("Link not fount")
	}
	return l
}


func NewLink() *Link {
	return &Link{Driver: driver}
}

type Product struct {
	Id					int64	`db:"id"`
	Name 				string	`db:"name"`
	Description			string	`db:"description"`
	Article				string	`db:"article"`
	NewPrice			float32	`db:"new_price"`
	OldPrice			float32	`db:"old_price"`
	Profit				float32	`db:"profit"`
	MinPrice			float32	`db:"min_price"`
	PrimeCost			float32	`db:"prime_cost"`
	Photos				string	`db:"photos"`
	Driver
}

func (p *Product) Find(id int64) *Product {
	p.db.Get(&*p, "SELECT * FROM product WHERE id = $1", id)
	return p
}


func NewProduct() *Product {
	return &Product{Driver: driver}
}

type ReplaceProduct struct {
	Id					int64	`db:"id"`
	Name 				string	`db:"name"`
	Description			string	`db:"description"`
	Key					string	`db:key`
	ProductId			int64	`db:key`
	Driver
}

/**
 * Получить продукт замену
 */
func (rp *ReplaceProduct) Find(id int64) *ReplaceProduct {
	rp.db.Get(&*rp, "SELECT * FROM replacement_products WHERE id = $1", id)
	return rp
}

/**
 * Получить продукт замену по ключу
 */
func (rp *ReplaceProduct) GetByKey(key string) *ReplaceProduct {
	err := rp.db.Get(&*rp, "SELECT * FROM replacement_products WHERE key = $1", key)
	common.InitError(err)
	return rp
}


func NewReplaceProduct() *ReplaceProduct {
	return &ReplaceProduct{Driver: driver}
}


type InsertLink struct {
	Id					int64	`db:"id"`
	Name				string	`db:"name"`
	Code				string	`db:"code"`
	Param 				string	`db:"param"`
	KeyValue 			string	`db:"key_value"`
	Driver
}

func (il *InsertLink) GetByParam(param string) *InsertLink {
	err := il.db.Get(&*il, "SELECT * FROM insert_links WHERE param = $1", param)
	common.InitError(err)
	return il
}


func NewInsertLink() *InsertLink {
	return &InsertLink{Driver: driver}
}

type ReplacementLink struct {
	Id					int64	`db:"id"`
	Name				string	`db:"name"`
	Code				string	`db:"code"`
	Param 				string	`db:"param"`
	KeyValue 			string	`db:"key_value"`
	Driver
}

//
//
func (rl *ReplacementLink) GetByParam(param string) *ReplacementLink {
	err := rl.db.Get(&*rl, "SELECT * FROM replacement_links WHERE param = $1", param)
	common.InitError(err)
	return rl
}


func NewRaplacementLink() *ReplacementLink {
	return &ReplacementLink{Driver: driver}
}

type Domain struct {
	Id					int64			`db:"id"`
	Name				string			`db:"name"`
	Status				int8			`db:"status"`
	CompanyId			int64			`db:"company_id"`
	DeletedAt			sql.NullString	`db:"deleted_at"`
	CreatedAt			time.Time		`db:"created_at"`
	UpdatedAt			time.Time		`db:"updated_at"`
	Driver								`place`
}

func (d *Domain) GetByName(name string) *Domain {
	err := d.db.Get(&*d, "SELECT * FROM domains WHERE name = $1 AND deleted_at IS null AND status = 1 LIMIT 1", name)
	common.InitError(err)
	return d
}


func NewDomain() *Domain {
	return &Domain{Driver: driver}
}