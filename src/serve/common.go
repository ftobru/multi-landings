package main

import (
	"github.com/fatih/color"
)


type Common struct {
	log		bool
}

func (c *Common) SetLog(enable bool) *Common {
	c.log = enable
	return c
}

func (c Common) Log() bool {
	return c.log
}

/**
 * Error - panic
 */
func (c Common) InitError(err error) {
	if err != nil {
		c.ErrorPrint(err.Error())
	}
}

func (c Common) ErrorPrint(err string) {
	color.Red(err)
}

// Debug print
func (c *Common) DebugPrint(text string) {
	if common.Log() {
		color.Yellow(text)
	}
}
