package main

import (
	"strings"
	"encoding/json"
	"fmt"
)

const (
	GEO_ADDR_TAG 		= "<geo_addr></geo_addr>"
	GEO_PHONE_TAG 		= "<geo_phone></geo_phone>"
	PRODUCT_ID_TAG		= "<product_id></product_id>"
	PRODUCT_NAME_TAG 	= "<product_name></product_name>"
	PRODUCT_DESCRIPTION	= "<product_description></product_description>"
	PRODUCT_ARTICLE_TAG = "<product_article></product_article>"
	PRODUCT_NEW_PRICE	= "<product_new_price></product_new_price>"
	PRODUCT_OLD_PRICE	= "<product_old_price></product_ol_price>"
	PRODUCT_PHOTO		= "<product_photo_%v></product_photo_%v>"
	PRODUCT_DISCOUNT	= "<product_discount></product_discount>"

)



type Landing struct {
	Path 				string
	Body 				string
	BasePath			string
	CompanyId			int64
	Status				int
}


/**
 * Замена по времени
 */
func (land *Landing) TimeReplace(replacement_time ReplacementTime) *Landing {
	(*land).Body = strings.Replace(land.Body, replacement_time.Tag, replacement_time.Meaning, -1)
	common.DebugPrint("Time replace")
	return land
}

type ReplaceJson map[string]string

/**
 * Линк замена
 */
func (land *Landing) LinkReplace(replace_link *ReplacementLink) *Landing {
	var replace ReplaceJson
	json.Unmarshal([]byte(replace_link.KeyValue), &replace)
	for _, value := range replace {
		(*land).Body = strings.Replace(land.Body, replace_link.Code, value, -1)
	}
	common.DebugPrint("Replace Link")
	return land
}

/**
 * Вставка
 */
func (land *Landing) InsertReplace(insert_link *InsertLink) *Landing {
	var replace ReplaceJson
	json.Unmarshal([]byte(insert_link.KeyValue), &replace)
	for _, value := range replace {
		(*land).Body = strings.Replace(land.Body, insert_link.Code, value, -1)
	}
	common.DebugPrint("Insert link")
	return land
}

/**
 * Параметр замена
 */
func (land *Landing) ParamReplace(params []Params) *Landing {
	for _, param := range params {
		r := strings.NewReplacer(param.Tag, param.Meaning)
		(*land).Body = r.Replace(land.Body)
	}
	return land
}

/**
 * Гео-замена
 */
func (land *Landing) GeoReplace(geo ReplacementGeo) *Landing {
	r := strings.NewReplacer(GEO_ADDR_TAG, geo.Address, GEO_PHONE_TAG, geo.Phone)
	(*land).Body = r.Replace(land.Body)
	return land
}

/**
 * Продукт замена
 */
func (land *Landing) ProductReplace(product *Product,) *Landing {
	r := strings.NewReplacer(PRODUCT_ID_TAG, string(product.Id),
		PRODUCT_ARTICLE_TAG, product.Article,
		PRODUCT_NAME_TAG, product.Name,
		PRODUCT_DESCRIPTION, product.Description,
		PRODUCT_NEW_PRICE, fmt.Sprintf("%.6f", product.NewPrice),
		PRODUCT_OLD_PRICE, fmt.Sprintf("%.6f", product.OldPrice))
	(*land).Body = r.Replace(land.Body)
	var photos []string
	json.Unmarshal([]byte(product.Photos), &photos)
	for key, photo := range photos {
		(*land).Body = strings.Replace(land.Body, fmt.Sprintf(PRODUCT_PHOTO, key), photo, -1)
	}
	return land
}

/**
 * Получить путь до корня лендинга
 */
func (land *Landing) GetBasePath() string {
	basePath := strings.Split(land.Path, "/")
	(*land).BasePath = strings.Join(basePath[:len(basePath)-1], "/")
	return land.BasePath
}









